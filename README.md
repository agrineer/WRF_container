



Brief Description:

This tiny code constructs a huge [Docker container](https://www.docker.com) for implementing a [Weather, Research, and Forecast](http://www2.mmm.ucar.edu/wrf/users) (WRF) data generator for Agrineer.org's [Soil Moisture Estimator](https://agrineer.org/sme/sme.php) (SME) project. 

The container allows for multi-core WRF application on an automated basis providing daily modeled weather data for the SME project.

Please read contents below before building the container.

Requirements:

 - multi-core cpu, depending on number of deliverable geographical sectors.
     - eg. 8 cores can deliver about 8 geographical sectors daily using current desktop computers.
 - large disk capacity.
    - ~35GB just for container.
    - data capacity depends on maintenance practice.
 - OS platform which supports Docker software.
 
 - Time to construct: about 2 hours, depending on number of sectors implemented.

License:
Gnu Public License 3.0 (GPLv3)
-------------------------------------------------
Changes since last release:

- Implementing WRF 4.1 version with parameters addressing wind (gravity wave drag and topographic effects).
- ETo calculation bug found and corrected. Previous version over-estimated the ETo. See the preliminary weather station comparsion results for more detail.
- Reduced WRF output from 2.4GB to ~55MB per (uncompressed) domain by tailoring Registry files. You can edit these to your needs. 

-------------------------------------------------------------

NOTE: You can use this docker file as a guide in implementing 
a non-container version by following the Docker commands step-by-step. 

-----------------------------------------------------------------

NOTE: The agrineer and wrf directories are stubbed out with links and folders which get filled in as the container is constructed.

-----------------------------------------------------------------

NOTE: This container requires a large disk space (about 65GB), which includes OS, 3rd party packages, and mostly geo static data. This does not include daily weather input daily or output data.

You will need to determine if the disk partition housing the container is large enough.

If you think the partition is not large enough to accomodate the container disk requirements, follow the instructions below in the "After Build" section.

-----------------------------------------------------------------
How to create the container:

Go to or make a recipient directory and install a copy of the released code: 

```bash
   $ cd .../projects
   $ git clone https://gitlab.com/agrineer/WRF_container
```
or
```bash
   $ tar xvf WRF_container.tar
```
Change into top directory:
```bash
   $ cd WRF_container
```
You will need to modify the Dockerfile to set your timezone.
Edit the Dockerfile and look for:

```bash
# *** ADJUST TIME ZONE ***
# *** ENTER YOUR TIME ZONE HERE ***

RUN rm /etc/localtime && ln -s /usr/share/zoneinfo/America/Denver /etc/localtime
```
Replace:

"America/Denver" in the string "/usr/share/zoneinfo/America/Denver"

with your time zone. Look in /usr/share/zoneinfo to find your time zone. 
You will need to restart the cron daemon once inside the container, see instructions below.
----------------------------------------------------------------

NOTE: All currently defined sectors are provided in wrf/sectors. You can construct the container faster by removing the sectors (directory) you are not interested in. 
----------------------------------------------
Data delivery to the SME project:

The purpose of this container is to provide a way, for interested and capable parties, to contribute with modest computers, or with advanced compute servers, to the generation of data which helps with water management for crop production, world-wide.

Due to the high computational load to cover large amounts of the world, a BOINC-like strategy is required, where many computers contribute to the data generation, verified with multiple copies, etc.

For server security reasons, data contributors to our server must register with us. Currently, the registration process is to contact admin (at) agrineer.org if you would like to be a data contributor.

NOTE: You do not have to be registered to use this container. You can generate the data locally and use the available SME and GDC command-line programs (available at gitlab.com/agrineer) on regular GNU/Linux platforms. Go to agrineer/wrf/scripts and edit run_wrfgfs.py. Notch out the "upload.sh" command. 

To deliver data to the Agrineer.org website you will need to edit wrf/scripts/upload.sh and
insert the data you registered with at the top:
```bash
NAME="Your Name"
INST="Institution Name"
EMAIL=yourname@institution.org
```
NOTE: Editing the wrf/scripts/upload.sh file BEFORE building the container preserves
the registration data for rentry into the container. As well, if you know you will be uploading please uncomment out the "upload to web server" in the run_wrfgfs.py file.
-----------------------------------------------------------------
If you have been using Docker, you may want to clear previously built containers.
Place these aliases in .bashrc:

alias docker-clean-unused='docker system prune --all --force'
alias docker-clean-all='docker container stop $(docker container ls -a -q) && docker system prune -a -f'

and invoke as needed to reclaim disk space.

-----------------------------------------------------------------
Build the container:

```bash
$ docker build -t mint19_wrf4.1 . | tee build.txt
```
You can use any container tag (name) you like, almost. For example, something like  "-t mymint19_b20181125:ver2" would say that it was built on 2018-11-25, version 2, with the Mint 19 updates for that date. Note: beginning character must be lower case. Also note that on every build the image is up-to-date on packages.

In the container build there is much downloading and data expansion. In particular, when grabbing the static geo files it appears to be stalled, but most likely it's not. Also, there is a huge delay when changing ownership to user "agrineer" files. If it's not complaining, be patient.

-----------------------------------------------------------------
After building:

The WRF model also requires large disk capacity for input and output data, depending on maintenance.

While the docker image and WRF can be run without external volumes, input and output data will be lost when exiting the container; in addition to any other changes made inside the container, including OS upates. 

NOTE: Periodically recreate container in order to include OS updates. Input and output volumes, if linked as below, will be preserved but any changes in code, etc., will not be preserved. Go to dockerfile source directories to implement new code modifications for the container. Whatever code changes were made inside the container must now be made in the container source to be used again with the new OS. 

-----------------------------------------------------
In Linux/Debian platforms the docker universe is kept in /var/lib/docker. Link this directory to a partition that can accomodate the docker container. To make the input and output data persistent after exiting the container, first create the "volumes" to house the data:
```bash
$ docker volume create gfs_0.25
$ docker volume create wrf_output
```
List the volumes:
```bash
$ docker volume ls
```

Get more detail:
```bash
$ docker volume inspect gfs_0.25
$ docker volume inspect wrf_output
```
Follow the volume mount point to do maintenance (backup, removes, etc.) on the host.

To use the volumes in the container (per container name above) use:
```bash
$ docker run -v gfs_0.25:/home/agrineer/wrf/gfs_0.25 -v wrf_output:/home/agrineer/wrf/output -it mint19_wrf4.1
```
If you choose not to use the persistent volumes just use:
```bash
$ docker run -it mint19_wrf4.1
```
------------------------------------------------------------
How to link the docker directory to a partition that can accomodate the docker container:
 
After creating the container, stop the docker daemon, copy the docker home to a large partition, link, and restart the daemon. This is shown for Debian/Ubuntu/Mint below:

Stop and copy:
```bash
$ sudo service docker stop
$ sudo cp -Rp /var/lib/docker /YOUR_BIG_PARTITION/docker
```
Its good practice to verify the copy destination before removing the source:
```bash
$ sudo diff -r /var/lib/docker /YOUR_BIG_PARTITION/docker
```
Per Unix, if nothing is reported it's OK.

Remove source, link, and restart daemon:
```bash
$ sudo rm -rf /var/lib/docker
$ sudo ln -s /YOUR_BIG_PARTITION/docker /var/lib/docker
$ sudo service docker start
```
-----------------------------------------------------------------

Inside the container:

The above docker command will give you a root login into the container. 

Because the cron daemon started on UTC time, restart the cron daemon to sync up to local timezone (see above on how to change timezones) DO NOT SKIP THIS:
```bash
$ service cron stop
$ service cron start
```
It is good practice NOT to be root on a routine basis due to potential destructive and unrecoverable effects. Hence, all of the WRF actions are conducted as user "agrineer".

To become "agrineer":
```bash
$ su - agrineer
```
Set mpd.hosts
```bash
$ echo `hostname` > mpd.hosts
```

-----------------------------------------------
Before running WRF for a sector you must get the forcing files:

Get yesterday's's input Global Forecast System (GFS) data run:
```bash
$ wrf/scripts/getdata_gfs.py
```
Get a past date GFS data:
```bash
$ wrf/scripts/getdata_gfs.py 20190101
```

Note: Warnings such as "No such directory 'pub/data/nccf/com/gfs/prod/gfs.2019011118' can be ignored as the script is asking for a future file.

Note: The Global Forecasting System server only holds data for about 9 days.

-----------------------------------------------------------------

How to run a sector with current GFS data (substitute your own sector below):
```bash
$ bin/run_wrf SECTOR
```
You can otherwise specify target date with past GFS data available. eg.:
```bash
$ bin/run_wrf SECTOR 20180515
```
The above WRF run takes about 4 hours using 4-cores depending on the weather described and CPU speed. Interestingly, the optimal split for an 8-core CPU is 4-core per sector. If you use 6 or 8 cores on a sector it will process quicker, but not enough to meet data delivery requirements throughout the day for 10 sectors.
10 sectors is the upper limit for a nominal desktop computer. 

You can monitor the initial WPS process with:
```bash
$ tail -f wrf/log/SECTOR.log
```
and when it gets to WRF implementation you can watch the WRF progress with:
```bash
$ tail -f wrf/sectors/SECTOR/wrf/rsl.error.0000
```
-------------------------------------------------------------

To change the number of cores/sector:
```bash
$ cd wrf/scripts
```
Edit wrfGFS.py, find the string "ncores = 4", change, and save.
Future releases will have "ncores" configurable as an input variable.

------------------------------------------------------------

Note: There is a lock file laid down in wrf/log for each WRF run. eg. wrf_running_SECTOR.log. Should the WRF run be interrupted for any reason you will need to remove the lock file before starting that SECTOR run again.

---------------------------------------------------------------

You can use crontab to run jobs for automated input data and WRF output generation. Edit cron/cronfile for local implementation then use:
```bash
$ crontab cron/cronfile
```

Verify cron jobs:
```bash
$ crontab -l
```
and wait for crontab to take effect.

OR:

Do not use crontab, but manually invoke WRF runs.

Recapping above:

For yesterday's run:
```bash
$ /home/agrineer/bin/run_wrf SECTOR
```
For past day run (w/GFS data):
```bash
$ /home/agrineer/wrf/scripts/getdata_gfs.py YYYYMMDD
$ /home/agrineer/bin/run_wrf SECTOR YYYYMMDD
```
When running, the program metgrid.exe will issue some warnings, visible in the sectors run logfile (found in /home/agrineer/wrf/log). From the on-line WRF tutorial:

"Note - When you run this (metgrid.exe), you will see a message at the end telling you the run was successful; however, you will also see a message that says "Note: The following floating-point exceptions are signalling: IIEE_UNDERFLOW_FLAG IEEE_DENORMAL". There is no need to be concerned about this message. This was put in for testing purposes and should not cause any problems." 

----------------------------------------------------------------
Tar'ed output for each run can be found in wrf/output/SECTOR/YYYYMMDD. If you are a data contributor to Agrineer.org's server then the data will be zipped and uploaded as well.
The WRF output can be saved for future reference. Look in the runwrf.py script to specify which domain is to be kept. 

-----------------------------------------------------------------
Licenses

All programs provided by Agrineer.org, a division of IndieCompLabs, LLC., are released under the GPLV3 license (see License.txt).

All other programs that are downloaded from official sites carry thier own licenses.

In particular, the UCAR/WRF Licence is downloaded on every container construction downloaded to the /home/agrineer/wrf directory. 



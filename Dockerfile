#  Dockerfile for Agrineer.org's WRF_container image
#  Copyright (C) 2018-2019 IndieCompLabs, LLC.
#                                                                               
#  This program is free software; you can redistribute it and/or modify         
#  it under the terms of the GNU General Public License as published by         
#  the Free Software Foundation; either version 3 of the License, or            
#  (at your option) any later version.                                          
#                                                                               
#  This program is distributed in the hope that it will be useful,              
#  but WITHOUT ANY WARRANTY; without even the implied warranty of               
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                
#  GNU General Public License for more details.                                 
#                                                                               
#  You should have received a copy of the GNU General Public License            
#  along with this program; if not, write to the Free Software                  
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.htm

# get base Mint image
FROM linuxmintd/mint19-amd64
MAINTAINER Scott L. Williams

# get preliminary goodies
# requests may be redundant but they get sorted out
RUN apt-get -y update \
    && apt-get -y dist-upgrade \
    && apt-get -y install \
       apt-utils \
       bash-completion \
       cron \
       curl \
       emacs \
       git \
       iputils-ping \
       net-tools \
       openssh-client \
       time \
       tzdata \
       unzip \
       wget \
       vim-common \
       vim-tiny \
       zip \
    && apt-get -y autoremove

# get WRF dependencies
RUN apt-get -y install \
    csh \
    gfortran \
    h5utils \
    hdf5-helpers \
    hdf5-tools \
    hdfview \
    libhdf5-dev \
    libhdf5-mpich-dev \
    libnetcdf-dev \
    libnetcdff-dev \
    libpng-dev \
    mpich \
    ncview \
    ncl-ncarg \
    netcdf-bin \
    python3-gdal \
    python3-netcdf4 \
    zlib1g-dev

# *** ADJUST TIME ZONE ***
# *** ENTER YOUR TIME ZONE HERE ***
RUN rm /etc/localtime && ln -s /usr/share/zoneinfo/America/Denver /etc/localtime

# create user agrineer and start to populate directory
RUN useradd -d /home/agrineer -m agrineer -s /bin/bash
RUN rm -rf /home/agrineer
ADD agrineer /home/agrineer
RUN chown -R agrineer.agrineer /home/agrineer

# first, as root, compile and install libjasper from source since
# it is no longer available as a package from Debian/Ubuntu
WORKDIR /usr/local/src
RUN curl -SLO http://www.ece.uvic.ca/~frodo/jasper/software/jasper-1.900.2.tar.gz \
    && sha256sum -c /home/agrineer/wrf/checksums --ignore-missing \
    && tar xfz jasper-1.900.2.tar.gz

WORKDIR /usr/local/src/jasper-1.900.2
RUN ./configure \
  && make \
  && make install

# become user agrineer
USER agrineer
WORKDIR /home/agrineer/wrf

# make input and out directories
RUN mkdir gfs_0.25 log output 

# get UCAR-BSD License and place in /home/agrineer/wrf
RUN curl -SL https://ral.ucar.edu/sites/default/files/public/projects/ncar-docker-wrf/ucar-bsd-3-clause-license.pdf > UCAR-BSD-3-Clause-License.pdf

# get current WRF source
RUN git clone https://github.com/wrf-model/WRF

# get current WPS source
RUN git clone https://github.com/wrf-model/WPS

# get static geo files, WPS_GEOG (about 30-40 minutes)
RUN curl -SLO http://www2.mmm.ucar.edu/wrf/src/wps_files/geog_high_res_mandatory.tar.gz \
   && sha256sum -c checksums --ignore-missing \
   && tar xfvz geog_high_res_mandatory.tar.gz \
   && rm geog_high_res_mandatory.tar.gz

# build WRF first
WORKDIR /home/agrineer/wrf/WRF

# first modify Registry files to reduce output data
# NOTE: should you want the default output (about 2.4GB/domain)
#       comment out the registry copy below
RUN cp -Rp Registry Registry.ORG && cp ../Registry/* Registry

# set up environment variables and build WRF
RUN cp ../run_wrf_configure . && ./run_wrf_configure

# next build WPS
WORKDIR /home/agrineer/wrf/WPS
RUN cp ../run_wps_configure . && ./run_wps_configure

# populate sector geo directories
WORKDIR /home/agrineer/wrf
RUN ./sector_geos

# become root for login
USER root
WORKDIR /root

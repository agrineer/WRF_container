&share
 wrf_core = 'ARW',
 max_dom = 3,
 start_date = '2016-10-12_06:00:00', '2016-10-12_06:00:00', '2016-10-12_06:00:00',
 end_date   = '2016-10-13_06:00:00', '2016-10-13_06:00:00', '2016-10-13_06:00:00',
 interval_seconds = 21600,
 io_form_geogrid = 2,
/

&geogrid
 parent_id         = 1,1,2,
 parent_grid_ratio = 1,3,3,
 i_parent_start    = 1,58,58,
 j_parent_start    = 1,58,58,
 e_we          = 172,172,172,
 e_sn          = 172,172,172,
 geog_data_res = '20m+default','20m+default','20m+default',
 dx = 30000,
 dy = 30000,
 map_proj =  'lambert',
 ref_lat   = 33.000,
 ref_lon   = -108.961,
 truelat1  = 30.000,
 truelat2  = 60.000,
 stand_lon = -108.961,
 geog_data_path = '/home/agrineer/wrf/WPS_GEOG',
/

&ungrib
 out_format = 'WPS',
 prefix = 'FILE',
/

&metgrid
 fg_name = 'FILE'
 io_form_metgrid = 2, 
/
